FROM ubuntu:20.04

LABEL maintainer="alex.strokach@utoronto.ca"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y \
    biber \
    bibtool \
    git \
    gosu \
    latexmk \
    locales \
    make \
    python-pygments \
    python3-pygments \
    texlive-full \
    tini \
    && ln -fs /usr/share/zoneinfo/America/Toronto /etc/localtime \
    && dpkg-reconfigure --frontend noninteractive tzdata \
    && rm -rf /var/lib/apt/lists/*

# Set an encoding to make things work smoothly
ENV LANGUAGE=en_US:en \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8

# Set environment variables
ENV USER=ubuntu \ 
    GROUP=ubuntu \
    USER_ID=1000 \
    GROUP_ID=1000 \
    HOME=/home/ubuntu \
    LOGNAME=ubuntu \
    MAIL=/var/spool/mail/ubuntu

# Create user
RUN groupadd -g $GROUP_ID -o $GROUP && \
    useradd --shell /bin/bash -u $USER_ID -g $GROUP_ID -o -c "" -m $USER

# Add entrypoint
COPY entrypoint_source /opt/docker/bin/entrypoint_source
COPY entrypoint /opt/docker/bin/entrypoint

# Ensure that all containers start with tini and the user selected process.
# Provide a default command (`bash`), which will start if the user doesn't specify one.
ENTRYPOINT [ "tini", "--", "/opt/docker/bin/entrypoint" ]
CMD [ "/bin/bash" ]
